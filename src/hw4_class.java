import java.util.Scanner;

public class hw4_class {

    public static void main(String[] args) {
        Scanner bb = new Scanner(System.in);

        System.out.println("Enter user name");
        String userName = bb.nextLine();

        System.out.println("Enter assistant name");
        String assistantName = bb.nextLine();

        System.out.println("Enter count of messages");
        int messageCount = bb.nextInt();

        System.out.println("Привет, " + userName + ", это твой помощник " + assistantName + ".");
        System.out.println("У тебя " + messageCount + " новых писем.");
    }

}
